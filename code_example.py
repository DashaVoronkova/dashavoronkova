import numpy as np
import pandas as pd
import math
from tqdm import tqdm
from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import ShuffleSplit
from sklearn.metrics import f1_score
import xgboost

train_features = pd.read_csv('train_hog.csv', header=None)
test_features = pd.read_csv('test_hog.csv', header=None)

def predict_single_label1(x_train, y_train, x_test, test_global):
    rf = RandomForestClassifier(n_estimators = 50)
    rf.fit(x_train, y_train)
    print('random forest fitted')
    pred = rf.predict(x_test)
    pred_global = rf.predict(test_global)
    return (pred, pred_global)

def predict_single_label2(x_train, y_train, x_test):
    clf = xgboost.XGBClassifier()
    clf.fit(x_train, y_train)
    print('xgboost fitted')
    pred = clf.predict(x_test)
    return pred

x_train = np.array(train_features)
y_train = train.iloc[:, 1:]
x_test = np.array(test_features)

ss = ShuffleSplit(n = x_train.shape[0], n_iter = 1, test_size=0.5)
for train_ids, test_ids in ss:
    x1_train = x_train[train_ids]
    y1_train = y_train.iloc[train_ids]
    x2_train = x_train[test_ids]
    y2_train = y_train.iloc[test_ids]

print('step 1')
all_pred1 = []
x_test_new = []
for i in tqdm(range(9)):
    label_name = 'label ' + str(i)
    y_train_label = np.array(y1_train[label_name])
    pred1, pred2 = predict_single_label1(x1_train, y_train_label, x2_train, x_test)
    all_pred1.append(pred1)
    x_test_new.append(pred2)

x2_train_new = np.swapaxes(np.array(all_pred1), 0, 1)
x_test_new = np.swapaxes(np.array(x_test_new), 0, 1)

print('step 2')
all_pred2 = []       
for i in tqdm(range(9)):
    label_name = 'label ' + str(i)
    y_train_label = np.array(y2_train[label_name])
    all_pred2.append(predict_single_label2(x2_train_new, y_train_label, x_test_new))

y_pred = np.swapaxes(np.array(all_pred2), 0, 1)

test_ids = photo_to_biz[(photo_to_biz['business_id'] > 1499)]
ids = test_ids['business_id'].unique()
labels = pd.DataFrame(y_pred)

def get_labels(x):
    ans = ''
    for i in range(9):
        if x.iloc[i] == 1:
            ans += str(i) + ' '
    return ans

labels['labels'] = labels.apply(get_labels, axis=1)
answer = pd.DataFrame({'business_id' : ids, 'labels' : labels['labels']})
answer.to_csv('answer.csv', index=None)